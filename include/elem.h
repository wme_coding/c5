//
// Created by Anton on 4/11/2020.
//

#pragma once

#include <ostream>
#include <list>
#include <vector>

class Elem{
    int value;
    int level;
    std::vector<int> path;
    Elem *leftElem;
    Elem *rightElem;

public:
    Elem(int value, int level);

    Elem();

    Elem(int value, int level, const std::vector<int> path);

    ~Elem();

    void setLeftElem(Elem *elem);

    void setRightElem(Elem *elem);

    bool hasLeft();

    bool hasRight();

    Elem &getLeftElem();

    Elem &getRightElem();

    int getLevel() const;

    void setValue(int value);

    int getAllEven();

    bool isPositive();

    void removeLeafs();

    int sum();

    int amountOfElems();

    std::vector<int> find(int x);

    friend std::ostream &operator<<(std::ostream &os, const Elem &elem);
};