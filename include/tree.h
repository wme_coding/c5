//
// Created by Anton on 4/11/2020.
//

#pragma once

#include "elem.h"
#include <list>
#include <ostream>

class BiTree{
    Elem head;

public:
    BiTree(int initialNumber);

    ~BiTree();

    void put(int x, const std::vector<int>& vector);

    int amountOfEven();

    bool isPositive();

    void removeLeafs();

    double average();

    std::vector<int> find(int x);

    friend std::ostream &operator<<(std::ostream &os, const BiTree &tree);
};
