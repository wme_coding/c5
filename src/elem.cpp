#include "../include/elem.h"

Elem::Elem(int value, int level) {
    this->value = value;
    this->level = level;
    this->leftElem = nullptr;
    this->rightElem = nullptr;
    this->path = {};
}

void Elem::setLeftElem(Elem *elem) {
    this->leftElem = elem;
}

void Elem::setRightElem(Elem *elem) {
    this->rightElem = elem;
}

Elem::Elem() {
    Elem(0, 0);
}

Elem & Elem::getLeftElem() {
    return *leftElem;
}

Elem & Elem::getRightElem() {
    return *rightElem;
}

bool Elem::hasLeft() {
    return leftElem != nullptr;
}

bool Elem::hasRight() {
    return rightElem != nullptr;
}

int Elem::getLevel() const {
    return level;
}

Elem::~Elem() {
    if(rightElem != nullptr){
        rightElem->~Elem();
        delete rightElem;
    }

    if(leftElem != nullptr){
        leftElem->~Elem();
        delete leftElem;
    }
}

std::ostream &operator<<(std::ostream &os, const Elem &elem) {
    if(elem.leftElem != nullptr){
        os << *elem.leftElem;
    }

    if(elem.level == 0){
        os << elem.value << " ";
    }

    if(elem.rightElem != nullptr){
        os << *elem.rightElem;
    }

    if(elem.level != 0)
        os << elem.value << " ";
    return os;
}

void Elem::setValue(int value) {
    this->value = value;
}

int Elem::getAllEven() {
    int count = 0;
    if(hasLeft()){
        count += leftElem->getAllEven();
    }

    if(hasRight()){
        count += rightElem->getAllEven();
    }

    if(value%2 == 0){
        count++;
    }

    return count;
}

bool Elem::isPositive() {
    bool res = true;
    if(hasLeft()){
        res = leftElem->isPositive();
        if(!res){
            return false;
        }
    }

    if(hasRight()){
        res = rightElem->isPositive();
        if(!res){
            return false;
        }
    }

    return value > 0;
}

void Elem::removeLeafs() {
    if(leftElem != nullptr){
        if(!leftElem->hasRight() and !leftElem->hasLeft()){
            delete leftElem;
            leftElem = nullptr;
        } else {
            leftElem->removeLeafs();
        }
    }

    if(rightElem != nullptr){
        if(!rightElem->hasRight() and !rightElem->hasLeft()){
            delete rightElem;
            rightElem = nullptr;
        } else {
            leftElem->removeLeafs();
        }
    }
}

int Elem::sum() {
    int sum = 0;

    if(hasLeft()){
        sum += leftElem->sum();
    }

    if(hasRight()){
        sum += rightElem->sum();
    }

    return sum + value;
}

int Elem::amountOfElems() {
    int amount = 0;

    if(hasLeft()){
        amount += leftElem->amountOfElems();
    }

    if(hasRight()){
        amount += rightElem->amountOfElems();
    }

    return amount + 1;
}

std::vector<int> Elem::find(int x) {
    std::vector<int> res = {};

    if(value == x){
        res =  this->path;
    }

    if(hasRight() and res.empty()){
        res = this->rightElem->find(x);
    }

    if(hasLeft() and res.empty()){
        res = this->leftElem->find(x);
    }

    return res;
}

Elem::Elem(int value, int level, const std::vector<int> path){
    this->value = value;
    this->level = level;
    this->leftElem = nullptr;
    this->rightElem = nullptr;
    this->path = path;
}

