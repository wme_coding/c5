#include <iostream>
#include "../include/tree.h"

BiTree::BiTree(int initialNumber) {
    this->head = Elem(initialNumber, 0);
}

//true - right
//false - left
void BiTree::put(int x, const std::vector<int>& vector) {
    bool check = true;
    Elem* ptr = &head;
    for(bool b: vector) {
        if (b) {
            if (ptr->hasRight()) {
                ptr = &ptr->getRightElem();
            } else {
                if (ptr->getLevel() == vector.size() - 1) {
                    ptr->setRightElem(new Elem(x, vector.size(), vector));
                    check = false;
                    break;
                } else {
                    std::cout << "Wrong Secuence" << std::endl;
                    check = false;
                    break;
                }
            }
        }

        if (!b) {
            if (ptr->hasLeft()) {
                ptr = &ptr->getLeftElem();
            } else {
                if (ptr->getLevel() == vector.size() - 1) {
                    ptr->setLeftElem(new Elem(x, vector.size(), vector));
                    check = false;
                    break;
                } else {
                    std::cout << "Wrong Secuence" << std::endl;
                    check = false;
                    break;
                }
            }
        }
    }
    if(check){
        ptr->setValue(x);
    }
}

BiTree::~BiTree() {
    head.~Elem();
}

std::ostream &operator<<(std::ostream &os, const BiTree &tree) {
    os << tree.head;
    return os;
}

int BiTree::amountOfEven() {
    return head.getAllEven();
}

bool BiTree::isPositive() {
    return head.isPositive();
}

void BiTree::removeLeafs() {
    head.removeLeafs();
}

double BiTree::average() {
    if(head.sum() == 0){
        std::cout << "Sum equals zero";
    }
    return (double)head.sum()/head.amountOfElems();
}

std::vector<int> BiTree::find(int x) {
    return head.find(x);
}






