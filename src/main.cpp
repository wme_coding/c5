#include <iostream>
#include "../include/tree.h"

using namespace std;

int main() {
    BiTree biTree(4);
    vector<int> dir;

    dir.push_back(1);
    biTree.put(22, dir);

    dir.clear();
    dir.push_back(0);
    biTree.put(25, dir);

    dir.clear();
    dir.push_back(0);
    dir.push_back(0);
    biTree.put(-79, dir);

    dir.clear();
    dir.push_back(0);
    biTree.put(1234, dir);

    cout << biTree << endl;

    cout << "Even elements: " << biTree.amountOfEven() << endl;

    cout << "Is Positive: " << biTree.isPositive() << endl;

    cout << "Average: " << biTree.average() << endl;

    std::vector<int> found = biTree.find(-79);
    for(int i = 0; i < found.size(); i++){
        cout << found[i] << " ";
    }
    cout << endl;

    biTree.removeLeafs();

    cout << biTree << endl;

    return 0;
}
